from flask import Flask
from flask import jsonify

from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/", methods=['GET'])
def index():
    data = [{"name": "Vasya", "age": "30"}, {"name": "Petya", "age": "50"}]
    return jsonify(data)
